import tkinter as tk
import STM as e
import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

nodes = []
elements = []
system = e.System()
thicness = 0
conClass = 0
c = 0
number_nodes = 0

#first window, to specify number of nodes:
window1 = tk.Tk()

window1.title('STM')
            
window1.geometry('500x200')

def submitNumberNodes():
    number=ent_number_nodes.get()
    if(number.isdigit()):
        global number_nodes
        number_nodes = int(number)
        window1.destroy()        

intro_string =  ("Welcome! This program will help you check if your strut-and-tie model is sufficient.\n" +
                   "This program will show you four windows in sequence. Each window will\n" +
                   "tell you what you need to know to fill in the required information. Good luck!")

lbl_intro = tk.Label(text =intro_string)

lbl_number_nodes = tk.Label(text="Enter number of nodes:")
ent_number_nodes = tk.Entry()
btn_submit = tk.Button(text="Submit", command = submitNumberNodes)

lbl_intro.pack()
lbl_number_nodes.pack()
ent_number_nodes.pack()
btn_submit.pack()

window1.mainloop()


#second window to setup the system
window2 = tk.Tk()

window2.title('System setup')
             
window2.geometry('500x200')

entry_x = [] #cordinates in x direction
entry_y = [] #cordinates in y direction
connected = [] #checkboxes
n=0
entry_force_magnitudes = [] #external forces
entry_force_widths = [] #external forces widths
error_text = []
error_lables = []
satisfied = True


def submitNodes():
    global error_text
    error_text = []
    global satisfied
    satisfied = True
    global nodes
    nodes.clear()
    global elements
    elements.clear()
    sum_force_magnitude = 0
    
    for i in range(number_nodes):
        force_magnitude = entry_force_magnitudes[i].get()
        force_width = entry_force_widths[i].get()
        if(force_magnitude==""):
            force_magnitude = 0
            force_width = 0
        elif(force_magnitude.isdigit() or (force_magnitude[0]=="-" and force_magnitude[1:len(force_magnitude)].isdigit())):
            if(force_width.isdigit()):
                force_magnitude = int(force_magnitude)
                force_width = int(force_width)
            else:
                error_text.append("Force width must be filled with integer number when force magnitude is applied.")
                print("Force width must be filled with integer number when force magnitude is applied.")
                satisfied = False
                break
        else:
            error_text.append("The magnitude must be filled with integer number.")
            print("The magnitude must be filled with integer number.")
            satisfied = False
            break
        sum_force_magnitude += force_magnitude
        node_position = []
        x = entry_x[i].get()
        y = entry_y[i].get()
        val1 = True
        try:
            x = int(x)
            y = int(y)
        except:
            val1 = False
        if(val1):
            satisfied = True
            node_position.append(x)
            node_position.append(y*-1)
            if(force_magnitude<0):
                node = e.Node(node_position,forceMagnitude_=abs(force_magnitude),forceWidth_=force_width)
                nodes.append(node)
            else:
                node = e.Node(node_position,forceMagnitude_=force_magnitude,forceWidth_=force_width,forceAngle_=3*np.pi/2)
                nodes.append(node)
        else:
            error_text.append("All positions need to be filled with integer number")
            print("All positions need to be filled with integer number")
            satisfied = False
            break
        
    connected_index=0
    
    if(sum_force_magnitude !=0):
        error_text.append("The structure needs to be at vertical equilibrium")
        satisfied = False
    
    if(len(error_lables)>0):
        for l in error_lables:
            l.destroy()
        error_lables.clear()
    
    if(satisfied==False):
        for t in error_text:
            label_error=tk.Label(text = t)
            label_error.pack()
            error_lables.append(label_error)
    
    if(satisfied):
        for i in range(number_nodes-1):
            for j in range(number_nodes-1-i):
                element = []
                if(connected[connected_index].get()):
                    element.append(nodes[i])
                    element.append(nodes[j+i+1])
                    elements.append(element)
                connected_index+=1
        
        for node in nodes:
            global system
            system.addNode(node)
        
        for element in elements:
            system.addElementWithNodes(element[0], element[1])
        e.ElementForce(system) #calculating the internal forces of the elements
        window2.destroy()

label = tk.Label( text="Input: Node cordinates, connected node(s), force at node.\n" + 
                 "Right handed cordinate system, positiv x to the rigth and positive y upwards.")
label.pack(padx=1, pady=1)
grid_frame = tk.Frame(window2)

#creating the grid for the nodes:
for i in range(number_nodes+2):
    
    grid_frame.columnconfigure(i, weight=1)
    grid_frame.rowconfigure(i, weight=1)
    
    for j in range(number_nodes+6):
        frame = tk.Frame(master=grid_frame)
        frame.grid(row=i, column=j,padx=1, pady=1)
        if(i==0):
            if(j==0):
                label = tk.Label(master=frame, text="Nodes")
                label.pack()
            elif(j==1):
                label = tk.Label(master=frame, text="X [mm]")
                label.pack()
            elif(j==2):
                label = tk.Label(master=frame, text="Y [mm]")
                label.pack()
            elif(j>2 and j<number_nodes+3):
                label = tk.Label(master=frame, text=j-2)
                label.pack()
            elif(j==number_nodes+4):
                label = tk.Label(master=frame, text="Force magnitude [N]")
                label.pack()
            elif(j==number_nodes+5):
                label = tk.Label(master=frame, text="Force width [mm]")
                label.pack()
        if(j==0 and i>0 and i<number_nodes+1):
            label = tk.Label(master=frame, text=i)
            label.pack()
        if(j==1 and i>0 and i<number_nodes+1):
            ent_number_x = tk.Entry(master=frame,width = 10)
            ent_number_x.pack()
            entry_x.append(ent_number_x)
        if(j==2 and i>0 and i<number_nodes+1):
            ent_number_y = tk.Entry(master=frame,width = 10)
            ent_number_y.pack()
            entry_y.append(ent_number_y)
        if(i>=1 and i<number_nodes+1 and j>i+2 and j<number_nodes+3):
            var = tk.IntVar()
            connected.append(var)
            check = tk.Checkbutton(master=frame,variable = connected[n])
            check.pack()
            n+=1
        if(i > 0 and i<number_nodes+1 and j == number_nodes+4):
            ent_force_magnitude = tk.Entry(master=frame,width = 18)
            ent_force_magnitude.pack()
            entry_force_magnitudes.append(ent_force_magnitude)
        if(i > 0 and i<number_nodes+1 and j == number_nodes+5):
            ent_force_width = tk.Entry(master=frame,width = 18)
            ent_force_width.pack()
            entry_force_widths.append(ent_force_width)
        if(i == number_nodes+1 and j == number_nodes+5):
            btn_submit_1 = tk.Button(master=frame,text="Submit", command = submitNodes)
            btn_submit_1.pack() 

grid_frame.pack(  fill = tk.BOTH )

window2.mainloop()


#window three to specify the concrete
window3 = tk.Tk()

window3.title('Concrete specification')
             
window3.geometry('500x200')

def submitInputs():
    global thicness
    global conClass
    global c
    val = True
    
    
    if(ent_concrete_class.get().isdigit()):
        conClass = int(ent_concrete_class.get())
    else:
        val=False
    if(ent_concrete_cover.get().isdigit()):
        c = int(ent_concrete_cover.get())
    else:
        val=False
    if(ent_thicness.get().isdigit()):
        thicness = int(ent_thicness.get())
    else:
        val=False
    if(val):
        window3.destroy()
    
        
lbl_instruction = tk.Label(text="All the numbers needs to be a integer number")
lbl_concrete_class = tk.Label(text="Enter the concrete class:")
ent_concrete_class = tk.Entry()
lbl_thicness = tk.Label(text="Enter the breadth of the concrete structure in mm:")
ent_thicness = tk.Entry()
lbl_concrete_cover = tk.Label(text="Enter the distance from concrete surface to center of tie in mm:")
ent_concrete_cover = tk.Entry()
btn_submit2 = tk.Button(text="Submit", command = submitInputs)

lbl_instruction.pack()
lbl_concrete_class.pack()
ent_concrete_class.pack()
lbl_thicness.pack()
ent_thicness.pack()
lbl_concrete_cover.pack()
ent_concrete_cover.pack()

btn_submit2.pack()

window3.mainloop()

checks = e.Checks(system, thicness, conClass, c) #do the checks for the stm

#window four to show results

window4 = tk.Tk()

window4.title('Results')

lbl_frame = tk.Frame(window4)
plt_frame = tk.Frame(window4)

output = checks.output_strings

for string in output:
    label=tk.Label(master = lbl_frame,text = string)
    label.pack()

plot = e.Plot(system)
fig = plot.draw()

canvas = FigureCanvasTkAgg(fig, master = plt_frame)
canvas.draw()
canvas.get_tk_widget().pack()

lbl_frame.pack(side=tk.LEFT)
plt_frame.pack(side=tk.RIGHT)

window4.mainloop()