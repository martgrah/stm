import numpy as np
import sys
sys.path.append('../')

import STM as m

#Creating the system:
system = m.System() 

#Creating the nodes:
node1 = m.Node([0,0]) 
node2 = m.Node([3000,0])
node3 = m.Node([1500,-1500])

#Applying force to the nodes:
node1.addForce(10000,400,3*np.pi/2)
node2.addForce(10000,400,3*np.pi/2)
node3.addForce(20000,400)

#Adding the nodes to the system:
system.addNode(node1)
system.addNode(node2)
system.addNode(node3)

#Create/add the elements:
element1 = m.Element(node1, node2)

system.addElementWithElement(element1)
system.addElementWithElement(m.Element(node2, node3))
system.addElementWithNodes(node1,node3)

#Calculating the element force:
e = m.ElementForce(system)

#Do the design checks:
m.Checks(system, 400, 30, 50)

#Plotting the results:
m.Plot(system)

