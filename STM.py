import numpy as np
import math as mt
import numpy.linalg as la 

class Node:
    def __init__(self,nodePosition_,forceMagnitude_= 0,forceWidth_= 0,
                 forceAngle_=np.pi/2):
    #The nodePosition must be a list that represent the coordinates to the node
    #in integer numbers. The rest of the arguments should be integer numbers.
        if(isinstance(nodePosition_, list)):
            if(len(nodePosition_)==2):
                if(isinstance(nodePosition_[0], int) and 
                   isinstance(nodePosition_[0], int)):
                    self.nodePosition = nodePosition_
                    self.nodeX = nodePosition_[0]
                    self.nodeY = nodePosition_[1]
                else:
                    print("The node list needs to contain integer numbers")
            else:
                print("The node list has to be of length 2")
        else:
            print("Then nodePosition needs to be of type list")
        self.forceMagnitude = 0
        self.forceAngle = 0
        self.forceWidth = 0
        self.isOkay=True
        if(forceMagnitude_!=0):
            self.addForce(forceMagnitude_,forceWidth_,forceAngle_)
        
    def addForce(self,forceMagnitude_,forceWidth_,forceAngle_=np.pi/2): 
        #adds force to node
            if(isinstance(forceMagnitude_, int) and 
               isinstance(forceAngle_, float) and 
               isinstance(forceWidth_, int)):
                self.forceMagnitude = forceMagnitude_
                self.forceAngle = forceAngle_
                self.forceWidth = forceWidth_
                self.forceX = self.forceMagnitude*-mt.cos(forceAngle_)
                self.forceY = self.forceMagnitude*mt.sin(forceAngle_)
            else:
                print("The forceMagnitude and forceWidth needs to be int," +
                      " and the forceAngle float")       
        
    def nodeType(self,system_): #determines node type
        system=system_
        elements=system.elementsFromNode(self)
        ties=0
        for elem in elements:
            if elem.forceMagnitude < 0:
                ties+=1
        if (len(elements) >= 3 and 
            abs(self.forceMagnitude) >0) or len(elements)> 3 : 
            #checks if more than 3 trusses at node
            if ties == 2:
                tieang=[]
                for elem in elements:
                    if elem.forceMagnitude < 0:
                        ang=angle([elem.node2.nodePosition[0]-
                                   elem.node1.nodePosition[0],
                                   elem.node2.nodePosition[1]-
                                   elem.node1.nodePosition[1]])
                        tieang.append(ang)
                if tieang[0]+tieang[1] == 0 or tieang[0]+tieang[1] == np.pi: 
                    #if two ties are paralell, they count as one
                    ties-=1        
        if ties==0:
            self.nodeType="CCC"
        elif ties==1:
            self.nodeType="CCT"
        else:
            self.nodeType="CTT"


class Element: #setup of element class, representing the trusses
    def __init__(self,node1_,node2_):
        self.node1 = node1_
        self.node2 = node2_
        self.width = []
        self.isOkay=True
          
    def returnOtherNode(self,node_): #return the other node of the element
        if(node_ == self.node1):
            return self.node2
        elif(node_ == self.node2):
            return self.node1
        else:
            print('not valid node')
    
    def addForce(self,forceMagnitude_): #adds force to the element
        self.forceMagnitude = forceMagnitude_
          
    def addWidth(self,width): #adds width to element in a list
        self.width.append(width)  
            
        
class System: #setup for the STM system
    def __init__(self):
        self.nodes = [] #The list of all nodes in the system
        self.elements = [] #The list of all elements in the system
        
    def addNode(self,node_): #adds nodes to the system
        if(self.existingNode(node_.nodePosition) == False):
            self.nodes.append(node_)
        else:
            print("This node already exist!")
            
    def addElementWithNodes(self,node1_,node2_): #adds elements to the system
        element = Element(node1_, node2_)
        if(self.existingNode(node1_.nodePosition) == False):
            print("This node1 does not exist. Creat node first")
        elif(self.existingNode(node2_.nodePosition) == False):
            print("This node2 does not exist. Creat node first")
        elif(self.existingElement(element)):
            print("This element already exist")
        else:
            self.elements.append(element)
                
    def addElementWithElement(self,element_): #adds elements to the system
        node1 = element_.node1
        node2 = element_.node2
        self.addElementWithNodes(node1, node2)
        
    def connectedNodes(self,node_): 
        #returns which nodes are connected to another node by an element
        connectedNodes = []
        if(self.existingNode(node_.nodePosition) != False):
            for element in self.elements:
                if(element.node1.nodePosition == node_.nodePosition):
                    connectedNodes.append(element.node2)
                elif(element.node2.nodePosition == node_.nodePosition):
                    connectedNodes.append(element.node1) 
            return connectedNodes
        else:
            print("This node is not part of the system")
            
    def elementsFromNode(self,node_): 
        #returns which elements are connected to a node
        connectedElements = []
        if(self.existingNode(node_.nodePosition)):
            for element in self.elements:
                if (node_.nodePosition == element.node1.nodePosition 
                    or node_.nodePosition == element.node2.nodePosition):
                    connectedElements.append(element)
            return connectedElements
        else:
            print("This node is not part of the system")         
        
    def existingNode(self,nodePosition_): 
        #checks if a node exist in the system
        for node in self.nodes:
            if(node.nodePosition == nodePosition_):
                return node
        return False
    
    def existingElement(self,element_): 
        #checks if an element exist in the system
        for element in self.elements:
            if((element.node1.nodePosition==element_.node1.nodePosition and 
                element.node2.nodePosition==element_.node2.nodePosition) or
               (element.node2.nodePosition==element_.node1.nodePosition and 
                element.node1.nodePosition==element_.node2.nodePosition)):
                return element
        return False
                
    
class ElementForce: #calculates the forces in the trusses
    def __init__(self,system_):
        self.system = system_
        self.solve()
        self.addTypes()
          
    def elementMatrix(self): 
        #setup of matrix with the contributions from the trusses on the nodes
        nNodes = len(self.system.nodes)
        nElements = len(self.system.elements)
        elementMatrix = np.zeros((nNodes*2,nElements)) #creating the matrix     
        for i in range(nNodes*2):
            if(i%2 == 0): #forces in x direction
                iNode = int(i/2)
                node = self.system.nodes[iNode]
                connectedElements = self.system.elementsFromNode(node)
                for iElement in range(nElements):
                    if(self.system.elements[iElement] in connectedElements):
                        element = self.system.elements[iElement]
                        elementMatrix[i][iElement] = self.xDirection(node, element.returnOtherNode(node))
        
            elif(i%2 == 1): #forces in y direction
                 iNode = int((i-1)/2)
                 node = self.system.nodes[iNode]
                 connectedElements = self.system.elementsFromNode(node)
                 for iElement in range(nElements):
                     if(self.system.elements[iElement] in connectedElements):
                         element = self.system.elements[iElement]
                         elementMatrix[i][iElement]=self.yDirection(node, element.returnOtherNode(node))
        return elementMatrix   
        
    def xDirection (self,node1_,node2_): #calculates the contribution in x-direction
        x1=node1_.nodeX
        x2=node2_.nodeX
        y1=node1_.nodeY
        y2=node2_.nodeY
        L = mt.sqrt((x2-x1)**2+(y2-y1)**2)
        if L == 0: #failsafe
            return 0
        return (x2-x1)/L   
    
    def yDirection (self,node1_,node2_): #calculates contribution i y-direction
        x1=node1_.nodeX
        x2=node2_.nodeX
        y1=node1_.nodeY
        y2=node2_.nodeY
        L=mt.sqrt((x2-x1)**2+(y2-y1)**2)
        if L == 0: #failsafe
            return 0
        return (y2-y1)/L 
    
    def forceMatrix(self): #setup of matrix of external forces on the nodes
        forceMatrix = np.zeros(len(self.system.nodes)*2)
        i = 0   
        for node in self.system.nodes:
            if(node.forceMagnitude):
                forceMatrix[i] = node.forceX
                forceMatrix[i+1] = node.forceY
            i+=2
        return forceMatrix  
    
    def solve(self): #solves the matrix equation, and add the truss forces into the elements
        elementMatrix = self.elementMatrix()
        forceMatrix = self.forceMatrix()
        self.elementForce = la.lstsq(elementMatrix, forceMatrix , rcond=None)
        res=False
        for force in self.system.nodes:
            l=len(str(int(force.forceMagnitude)))
            if self.elementForce[1] > 10**(l-1) and l !=1:
                res=True
        if res: #checks if residual is too high
            self.system.residual="\nHigh residual in the calculation of forces, please check equilibrium!!"
        else:
            self.system.residual=""
        i = 0
        for force in self.elementForce[0]:
            self.system.elements[i].addForce(force)
            i+=1
        return self.elementForce  

    def addTypes(self): #adds the nodetypes to the node objects after the trussforces have been calculated
        for node in self.system.nodes:
            node.nodeType(self.system)        
            

class Checks:
    def __init__(self,system_,thicness_,conClass_,c_): 
        #performs design checks for all nodes and elements in the system
        self.system=system_
        self.thickness=thicness_
        self.conClass=conClass_*0.85/1.5
        self.v=1-self.conClass/250
        self.c=c_    
        self.output_strings = []
        self.is_okay=True
        self.check()
        string=self.system.residual
        self.output_strings.append(string)      
        print(string)
        
    def check(self):
        num=1  #nodenumber
        for node in self.system.nodes:
            elements=self.system.elementsFromNode(node)
            morethan3=False
            if (len(elements) >= 3 and abs(node.forceMagnitude) >0) or len(elements)> 3 : #checks if more than 3 forces act on the node
                morethan3=True
            if node.nodeType=="CCC":
                isflat=False
                for elem in elements: #checks if one of the struts are flat
                    vector=[elem.node2.nodePosition[0]-elem.node1.nodePosition[0],elem.node2.nodePosition[1]-elem.node1.nodePosition[1]]
                    ang=angle(vector)
                    
                    if ang==0:
                        isflat=True
                        flat=elem       
                if node.forceMagnitude: #if external force exist
                    F1=node.forceMagnitude
                    width1=node.forceWidth
                    if isflat:
                        F2=flat.forceMagnitude
                        width2=2*self.c         #assumes height of flat strut to be 2* cover
                        flat.addWidth(width2) 
                        comp=[] #components
                        ang=[]  #angles
                        for elem in elements:
                            if elem != flat:
                                comp.append(elem)
                                node2=elem.returnOtherNode(node)
                                vector=[node2.nodePosition[0]-node.nodePosition[0],node2.nodePosition[1]-node.nodePosition[1]]
                                ang.append(angle(vector)) 
                        if morethan3: #calculates the resultant if more than 3 forces
                            R=0
                            vert=0
                            hor=0
                            i=0
                            while i < len(comp):
                                R+=comp[i].forceMagnitude**2
                                vert+=comp[i].forceMagnitude*mt.sin(ang[i])
                                hor+=comp[i].forceMagnitude*mt.cos(ang[i])
                                i+=1
                            R=mt.sqrt(R)
                            ang=abs(mt.atan(vert/hor))
                            F3=R
                            width3=width1*mt.sin(ang)+width2*mt.cos(ang)
                            node.isOkay=self.CCC(F1, F2, F3, width1, width2, width3, self.thickness, self.conClass, self.v, num)
                        else: #don't need a resultant
                            F3=comp[0].forceMagnitude
                            width3=abs(width1*mt.sin(ang[0]))+abs(width2*mt.cos(ang[0])) 
                            comp[0].addWidth(width3)
                            node.isOkay=self.CCC(F1, F2, F3, width1, width2, width3, self.thickness, self.conClass, self.v, num)
                    else:  #isn't flat
                        comp=[]
                        ang=[]
                        for elem in elements:
                            comp.append(elem)
                            node2=elem.returnOtherNode(node)
                            vector=[node2.nodePosition[0]-node.nodePosition[0],node2.nodePosition[1]-node.nodePosition[1]]
                            angtemp=angle(vector)
                            if angtemp > np.pi/2: angtemp=np.pi-angtemp
                            ang.append(angtemp)
                        F2=comp[0].forceMagnitude
                        width2=width1*mt.sin(ang[0])/(mt.sin(np.pi-ang[0]-ang[1]))
                        F3=comp[1].forceMagnitude
                        width3=width1*mt.sin(ang[1])/(mt.sin(np.pi-ang[0]-ang[1]))
                        comp[0].addWidth(width2) 
                        comp[1].addWidth(width3) 
                        node.isOkay=self.CCC(F1, F2, F3, width1, width2, width3, self.thickness, self.conClass, self.v, num)
                else: #no external force, internal CCC node is smeared
                    string = "Node "+str(num)+" is a smeered node, no check needed"
                    print(string)
                    self.output_strings.append(string)
            elif node.nodeType=="CCT":
                if node.forceMagnitude > 0: #if external force exist
                    F1=node.forceMagnitude
                    width1=node.forceWidth
                    if morethan3:
                        comp=[]
                        ang=[]
                        strut=0
                        tie=0
                        for elem in elements:
                            if elem.forceMagnitude < 0:
                                tie+=1
                            else:
                                strut+=1
                                comp.append(elem)
                                node2=elem.returnOtherNode(node)
                                vector=[node2.nodePosition[0]-node.nodePosition[0],node2.nodePosition[1]-node.nodePosition[1]]
                                angtemp=angle(vector)
                                ang.append(angtemp)  
                        if strut==2:  #2 struts at CCT node with external force, checked twice, once for each side
                            width2_1=width1*np.sin(ang[0])+2*self.c*np.cos(ang[0]) #assumes height of tie to be 2* cover
                            width2_2=width1*np.sin(ang[1])+2*self.c*np.cos(ang[1])
                            F2_1=comp[0].forceMagnitude
                            F2_2=comp[1].forceMagnitude
                            comp[0].addWidth(width2_1) 
                            comp[1].addWidth(width2_2) 
                            node.isOkay=self.CCT(F1, F2_1, self.thickness, width1, width2_1, self.conClass, self.v, num)
                            node.isOkay=self.CCT(F1, F2_2, self.thickness, width1, width2_2, self.conClass, self.v, num)
                        else:
                            string = "please have a maximum of 2 struts at node " +str(num) 
                            print(string)
                            self.output_strings.append(string)
                    else: #if not more than 3
                        for elem in elements:
                            if elem.forceMagnitude > 0:
                                node2=elem.returnOtherNode(node)
                                vector=[node2.nodePosition[0]-node.nodePosition[0],node2.nodePosition[1]-node.nodePosition[1]]
                                ang=angle(vector)
                                F2=elem.forceMagnitude
                                width2=width1*np.sin(ang)+2*self.c*np.cos(ang) #assumes  heigth of tie to be 2* cover
                                elem.addWidth(width2)
                        node.isOkay=self.CCT(F1, F2, self.thickness, width1, width2, self.conClass, self.v, num)
                else: #no external force
                    if morethan3:
                        comp=[]
                        ang=[]
                        strut=0
                        isflat=False
                        for elem in elements: 
                            if elem.forceMagnitude > 0:
                                strut+=1
                                comp.append(elem)
                                node2=elem.returnOtherNode(node)
                                vector=[node2.nodePosition[0]-node.nodePosition[0],node2.nodePosition[1]-node.nodePosition[1]]
                                angtemp=angle(vector)
                                ang.append(angtemp)
                                if angtemp==0:
                                    isflat=True
                                    flat=elem
                                    comp.remove(flat)
                        if strut==3: 
                            if isflat:
                                F1=flat.forceMagnitude
                                width1=2*self.c
                                flat.addWidth(width1)
                                R=0
                                vert=0
                                hor=0
                                i=0
                                while i < len(comp):
                                    R+=comp[i].forceMagnitude**2
                                    vert+=comp[i].forceMagnitude*mt.sin(ang[i])
                                    hor+=comp[i].forceMagnitude*mt.cos(ang[i])
                                    i+=1
                                R=mt.sqrt(R)
                                ang=abs(mt.atan(vert/hor))
                                F2=R
                                width2=width1*mt.sin(ang)+width2*mt.cos(ang)
                                node.isOkay=self.CCT(F1, F2, self.thickness, width1, width2, self.conClass, self.v, num)
                            else:
                                string = "Node "+str(num)+" is a smeared node, no check needed"
                                print(string)
                                self.output_strings.append(string)
                        else:
                            string = "Too many struts at node "+str(num)
                            print(string)
                            self.output_strings.append(string)
                    else: #3 trusses
                        comp=[]
                        ang=[]
                        isflat=False
                        for elem in elements: 
                            if elem.forceMagnitude > 0:
                                comp.append(elem)
                                node2=elem.returnOtherNode(node)
                                vector=[node2.nodePosition[0]-node.nodePosition[0],node2.nodePosition[1]-node.nodePosition[1]]
                                angtemp=angle(vector)
                                ang.append(angtemp)
                                if angtemp==0:
                                    isflat=True
                                    flat=elem
                                    comp.remove(flat)
                        if isflat:
                            F1=flat.forceMagnitude
                            width1=2*self.c
                            flat.addWidth(width1)
                            F2=comp[0].forceMagnitude
                            width2=width2=width1*mt.sin(ang[0])+width2*abs(mt.cos(ang[0]))
                            comp[0].addWidth(width2)
                            node.isOkay=self.CCT(F1, F2, self.thickness, width1, width2, self.conClass, self.v, num)
                        else: #isn't flat
                            string = "Node "+str(num)+" is a smeared node, no check needed"
                            print(string)
                            self.output_strings.append(string)
            else: #CTT node
                for elem in elements:
                    if elem.forceMagnitude > 0:
                        F1=elem.forceMagnitude
                        node2=elem.returnOtherNode(node)
                        vector=[node2.nodePosition[0]-node.nodePosition[0],node2.nodePosition[1]-node.nodePosition[1]]
                        ang=angle(vector)
                        width1=self.c*np.sin(ang)+2*self.c*np.cos(ang)
                        elem.addWidth(width1) 
                        node.isOkay=self.CTT(F1, self.thickness, width1, self.conClass, self.v, num)
            num+=1     #adds to the nodenumber  
        print('')
        self.output_strings.append('')
        num=1  #truss number
        for elem in self.system.elements:
                if elem.forceMagnitude >0:  #checks if strut or tie             
                    elem.isOkay=self.strut(elem, self.thickness, self.conClass, self.v, num)
                else:
                    self.tie(elem.forceMagnitude,num)
                num+=1 #adds to trussnumber
        if self.is_okay: #if the system doesnt fail
            string="\nThe capacity of the system is enough"
            print (string)
            self.output_strings.append(string)
            s=strainEnergy(self.system)         #calculate strain energy if the system holds
            string='\nThe strain energy in the system is ' +str(int(s))+' J'
            print (string)
            self.output_strings.append(string)   
        else:
            string="\nThe capacity of the system is NOT enough!!!"
            print (string)
            self.output_strings.append(string)
            
    def CCC(self, F1, F2, F3, width1, width2, width3, bredd, strength, v, num): 
        #design check of CCC node
        sigma_1=(F1/(bredd*width1))      #stresses
        sigma_2=(F2)/(bredd*width2)
        sigma_3=F3/(bredd*width3)      
        sigma_M=v*strength             #Ec2. formula(6.60) for ccc-node   
        if sigma_1 > sigma_M or sigma_2 > sigma_M or sigma_3 > sigma_M:
            string = "The capacity of node " +str(num)+" is not enough."
            print (string)
            self.output_strings.append(string)
            self.is_okay=False
            return False
        else:
            string = "The capacity of node " +str(num)+" is OK!"
            print(string) 
            self.output_strings.append(string)
            return True            
            
    def CCT(self, F1, F2, bredd, width1, width2, strength, v, num): #design check for CCT node
        sigma_1=F1/(width1*bredd)         #stresses
        sigma_2=F2/(width2*bredd)        
        sigma_M=0.85*v*strength        #Ec2. formula(6.61) for cct-node
        if sigma_1 > sigma_M or sigma_2 > sigma_M:
            string = "The capacity of node " +str(num)+" is not enough."
            print (string)
            self.output_strings.append(string)
            self.is_okay=False
            return False
        else:
            string="The capacity of node " +str(num)+" is OK!"
            print(string)
            self.output_strings.append(string)
            return True
            
    def CTT(self, F1, bredd, width1, strength, v, num): #design check for CTT node
        sigma_1=F1/(width1*bredd)       #stress  
        sigma_M=0.75*v*strength        #Ec2. formula(6.62) for ctt-node
        if sigma_1 > sigma_M:
            string = "The capacity of node " +str(num)+" is not enough."
            print (string)
            self.output_strings.append(string)
            self.is_okay=False
            return False
        else:
            string = "The capacity of node " +str(num)+" is OK!"
            print(string)
            self.output_strings.append(string)
            return True
            
    def strut(self, element, thickness, strength, v, num): 
        #design check of strut
        isokay=True
        sigma_M=0.6*v*strength  # Ec2. formula(6.56) for strut with transverse tensile stress 
        for i in element.width: #checking both ends of the strut
            sigma=element.forceMagnitude/(i*thickness)
            if sigma > sigma_M:
                isokay=False
         
        if isokay == False:
            string = "The capacity of strut " +str(num)+" is not enough."
            print (string)
            self.output_strings.append(string)
            self.is_okay=False
            return False
        else:
            string = "The capacity of strut " +str(num)+" is OK!" 
            print(string)
            self.output_strings.append(string)
            return True
            
    def tie(self,F,num): #calculate required reinforcement in the ties
        Area_s=round(abs(F)/434 +0.5)  
        string = "Required reinforcement area in tie "+str(num)+" is "+str(Area_s)+"mm^2"
        print(string)
        self.output_strings.append(string)


class Plot: #plots the system
    def __init__(self,system_):
        self.system=system_
        self.pl=self.draw()        

    def draw(self):
        arrows=True
        import matplotlib as mpl
        import matplotlib.pyplot as plt
        f = plt.figure()
        num=1 #nodenumber             
        for node in self.system.nodes: #plotting the nodes
           x=node.nodeX
           y=-node.nodeY
           cor="("+str(node.nodeX)+","+str(-node.nodeY)+")"
           if node.isOkay:
               plt.plot(x, y, 'ko') 
           else:
               plt.plot(x,y,'ro')   
           plt.annotate(num, xy=(x, y), xytext=(x+80, y-80), color='blue')
           plt.annotate(cor, xy=(x, y) , xytext=(x, y+150),color='blue')
           if arrows:
               if node.forceMagnitude !=0: #arrows, not fun might break
                       lolx=len(str(int(node.forceX)))
                       if abs(node.forceX) < 1:
                           x1=x
                           if node.forceX<0:
                               x1=x-(node.forceX*node.forceMagnitude/(10**(lolx-3.2)))
                           else:
                               x1=x+(node.forceX*node.forceMagnitude/(10**(lolx-3.2)))
                       loly=len(str(int(node.forceY*node.forceMagnitude)))
                       if node.forceAngle == (3*np.pi/2):
                           y1=y+(node.forceY*node.forceMagnitude/(10**(loly-4.2)))
                       else:
                           y1=y+(node.forceY*node.forceMagnitude/(10**(loly-3.2)))
                       plt.annotate(str(abs(node.forceMagnitude)), xy=(x, y), xytext=(x1, y1), arrowprops=dict(facecolor='black', shrink=0.05))   
           num+=1
        num=1 #element number
        for elem in self.system.elements:
            xmid=(elem.node1.nodeX+elem.node2.nodeX)/2
            ymid=(-elem.node1.nodeY+-elem.node2.nodeY)/2
            #angles of the plotted forces, and numbering of element
            if (elem.node1.nodeX>elem.node2.nodeX and elem.node1.nodeY < elem.node2.nodeY) or (elem.node2.nodeX>elem.node1.nodeX and elem.node2.nodeY < elem.node1.nodeY):
                ang=180-mt.degrees(angle([elem.node2.nodeX-elem.node1.nodeX,-elem.node2.nodeY--elem.node1.nodeY]))
                plt.annotate(num, xy=(x, y), xytext=(xmid+50,ymid-80))
            else:
                ang=-mt.degrees(angle([elem.node2.nodeX-elem.node1.nodeX,-elem.node2.nodeY--elem.node1.nodeY]))
                plt.annotate(num, xy=(x, y), xytext=(xmid-60,ymid-80))
            if elem.forceMagnitude<0:    
                plt.plot([elem.node1.nodeX,elem.node2.nodeX],[-elem.node1.nodeY,-elem.node2.nodeY],'k-')
                plt.annotate(round(elem.forceMagnitude), xy=(xmid,ymid), xytext=(xmid-110, ymid+50), rotation=ang)  
            else:
                if elem.isOkay:
                    plt.plot([elem.node1.nodeX,elem.node2.nodeX],[-elem.node1.nodeY,-elem.node2.nodeY],'k--')
                    plt.annotate(round(elem.forceMagnitude), xy=(xmid,ymid), xytext=(xmid-80, ymid+50), rotation=ang)  
                else:
                    plt.plot([elem.node1.nodeX,elem.node2.nodeX],[-elem.node1.nodeY,-elem.node2.nodeY],'r--')
                    plt.annotate(round(elem.forceMagnitude), xy=(xmid,ymid), xytext=(xmid-80, ymid+50), rotation=ang)     
            num+=1        
        plt.axis('off') #turn off axissystem
        plt.gca().set_aspect('equal', adjustable='box')
        f.set_size_inches(10, 5.6, forward=True) #set size of the figure
        return f



def strainEnergy(system): #calculates the total strain energy in the system
    strain=0
    for i in system.elements:
        if i.forceMagnitude < 0:
            l=mt.sqrt((i.node1.nodeX-i.node2.nodeX)**2+
                      (i.node1.nodeY-i.node2.nodeY)**2)
            strain+=abs(i.forceMagnitude)*l*434/200000*10**(-3)
    return strain
        


def angle(vector): #calculate angle from the horizontal to a vector
    vector_2=[mt.inf,0]
    ang1 = np.arctan2(*vector[::-1])
    ang2 = np.arctan2(*vector_2[::-1])
    ang=(ang2 - ang1) % (2 * np.pi)
    if ang >np.pi/2: ang=np.pi-ang
    return abs(ang)