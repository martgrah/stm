from STM import*
import numpy as np
system=System()

node1=Node([0,0])
node2=Node([-900,1300])
node3=Node([900,1300])

system.addNode(node1)
system.addNode(node2)
system.addNode(node3)

system.addElementWithNodes(node2, node1)
system.addElementWithNodes(node1, node3)
system.addElementWithNodes(node2, node3)

node2.addForce(1250000, 314, 3*np.pi/2)
node3.addForce(1250000, 314, 3*np.pi/2)
node1.addForce(2500000, 500)	

e = ElementForce(system)
Checks(system, 900, 30, 50)
Plot(system)

