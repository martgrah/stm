import sys
sys.path.append('../')

from STM import*
import numpy as np
system=System()

node1=Node([0,0])
node2=Node([1250,-2000])
node3=Node([5000,0])
node4=Node([3750,-2000])


system.addNode(node1)
system.addNode(node2)
system.addNode(node3)
system.addNode(node4)

system.addElementWithNodes(node1, node2)
system.addElementWithNodes(node1, node3)
system.addElementWithNodes(node2, node4)
system.addElementWithNodes(node3, node4)

node1.addForce(810000, 376, 3*np.pi/2)
node2.addForce(810000, 376)
node3.addForce(810000, 376, 3*np.pi/2)
node4.addForce(810000, 376)

e=ElementForce(system)
Checks(system, 250, 25, 180)
Plot(system)
