from STM import*
import numpy as np
system=System()

node1=Node([0,0])
node2=Node([1800,0])
node3=Node([900,-1500])
node4=Node([450,-1500])
node5=Node([1350,-1500])
node6=Node([450,0])
node7=Node([1350,0])

system.addNode(node1)
system.addNode(node2)
system.addNode(node3)
system.addNode(node4)
system.addNode(node5)
system.addNode(node6)
system.addNode(node7)

system.addElementWithNodes(node1, node4)
system.addElementWithNodes(node1, node6)
system.addElementWithNodes(node2, node5)
system.addElementWithNodes(node2, node7)
system.addElementWithNodes(node3, node4)
system.addElementWithNodes(node3, node5)
system.addElementWithNodes(node4, node6)
system.addElementWithNodes(node5, node7)
system.addElementWithNodes(node6, node7)
system.addElementWithNodes(node3, node6)
system.addElementWithNodes(node3, node7)

node1.addForce(1250000, 320, 3*np.pi/2)
node2.addForce(1250000, 320, 3*np.pi/2)
node3.addForce(2500000, 500)	

e=ElementForce(system)
Checks(system, 900, 30, 100)
Plot(system)
