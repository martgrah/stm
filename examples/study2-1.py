from STM import*
import numpy as np
system=System()

node1=Node([0,0])
node2=Node([1250,-3300])
node7=Node([4300,0])

system.addNode(node1)
system.addNode(node2)

system.addNode(node7)

system.addElementWithNodes(node1, node2)
system.addElementWithNodes(node1, node7)
system.addElementWithNodes(node2, node7)

node1.addForce(1794000, 500, 3*np.pi/2)
node2.addForce(2529000, 500)
node7.addForce(735000, 500, 3*np.pi/2)

e=ElementForce(system)
Checks(system, 500, 35, 100)
Plot(system)
